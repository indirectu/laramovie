<?php
/**
 * @package php-tmdb\laravel
 * @author Mark Redeman <markredeman@gmail.com>
 * @copyright (c) 2014, Mark Redeman
 */
return [
    /*
     * Api key
     */
    'api_key' => '210e865bbeaea792416b1f3e055fddb6',

    /**
     * Client options
     */
    'options' => [
        /**
         * Use https
         */
        'secure' => true,

        /*
         * Cache
         */
        'cache'  => [
            'enabled' => true,
            // Keep the path empty or remove it entirely to default to storage/tmdb
            'path'    => storage_path('tmdb')
        ],

        /*
         * Log
         */
        'log'    => [
            'enabled' => true,
            // Keep the path empty or remove it entirely to default to storage/logs/tmdb.log
            'path'    => storage_path('logs/tmdb.log')
        ],
    ],
    /*
     * Limitation rules
     */
    'limit'   => [
        'requests' => 40,
        // How many reuqests at every X seconds
        'interval' => 10
    ]
];
