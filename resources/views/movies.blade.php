<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #000;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="content">
    <div class="title m-b-md">
        <?php echo $page_title; ?>
    </div>

    <?php if (count($movies)): ?>
    <table border="1" align="center" width="80%" cellpadding="10">
        <tr>
            <th>Original title</th>
            <th>Genres</th>
            <th>Release date</th>
        </tr>
        <?php foreach ($movies as $movie): ?>
        <?php
        $genres_json = $movie['genres'];
        $genres_array = json_decode($genres_json, true);
        $genres_txt = implode(",", (array)$genres_array);
        ?>
        <tr>
            <td><?php echo $movie['original_title'] ?></td>
            <td><?php echo $genres_txt ?></td>
            <td><?php echo $movie['release_date'] ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>
</body>
</html>
