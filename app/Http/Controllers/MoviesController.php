<?php

namespace App\Http\Controllers;

use App\Tmdb\RequestsLimit;
use Tmdb\Api\Genres;
use Tmdb\Model\Query\Discover\DiscoverMoviesQuery;
use Tmdb\Repository\DiscoverRepository;

/**
 * Class MoviesController
 * @package App\Http\Controllers
 */
class MoviesController extends Controller
{
    private $movies;
    private $limit;
    private $genres;
    private $todayDate;
    private $movieStorage;

    /**
     * MoviesController constructor.
     * @param RequestsLimit $limit
     * @param DiscoverRepository $movies
     * @param Genres $genres
     */
    function __construct(RequestsLimit $limit, DiscoverRepository $movies, Genres $genres)
    {
        $this->movies       = $movies;
        $this->limit        = $limit;
        $this->genres       = $genres->getMovieGenres();
        $this->todayDate    = date('Y-m-d');
        $this->movieStorage = [];
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    function index()
    {
        $movies = $this->getTodayMovies();

        return view('movies', array(
            'page_title' => 'Movies',
            'movies'     => $movies
        ));
    }

    /**
     * Get movies from the current day
     */
    public function getTodayMovies()
    {
        $page  = 1;
        $pages = false;

        do {
            $query = new DiscoverMoviesQuery();
            $query
                ->page($page++)
                ->primaryReleaseDateGte($this->todayDate)
                ->primaryReleaseDateLte($this->todayDate);

            $movies = $this->movies->discoverMovies($query);

            foreach ($movies as $movie) {
                $movie_details = [
                    'original_title' => $movie->getOriginalTitle(),
                    'genres'         => $this->getGenres($movie->getGenres()->getGenres()),
                    'release_date'   => $this->getReleaseDate($movie->getReleaseDate())
                ];

                $this->movieStorage[] = $movie_details;

                // Add to database if not exist already
                \App\Movie::firstOrCreate(
                    $movie_details
                );
            }

            if ($pages === false) {
                $pages = $movies->getTotalPages();
            }
            $pages -= 1;

            // Handling API limitation rules
            $this->limit->check();

            // Updating requests counter
            $this->limit->requests++;

        } while ($pages);

        return $this->movieStorage;
    }

    /**
     * @param $release
     * @return mixed
     */
    public function getReleaseDate($date)
    {
        $o = new \ReflectionObject($date);
        $p = $o->getProperty('date');

        return $p->getValue($date);
    }

    /**
     * @param $genres
     * @return string
     */
    public function getGenres($obj)
    {
        $all_genres   = $this->genres;
        $genres_array = [];

        foreach ($all_genres['genres'] as $genre) {
            foreach ($obj as $o) {
                if ($o->getId() == $genre['id']) {
                    $genres_array[] = $genre['name'];
                }
            }
        }

        return (empty($genres_array)) ? null : json_encode($genres_array);
    }

}