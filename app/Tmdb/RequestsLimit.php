<?php

namespace App\Tmdb;

use Illuminate\Support\Facades\Config;

/**
 * Class RequestsLimit
 * @package App\Tmdb
 */
class RequestsLimit
{
    private $limit;

    /**
     * RequestsLimit constructor.
     */
    public function __construct()
    {
        $this->requests = 0;
        $this->limit    = (object)Config::get('tmdb.limit');
    }

    /**
     * Check requests and time interval
     * @return bool
     */
    public function check()
    {
        // Calculate how many seconds script has been running for until now
        if ($this->requests >= $this->limit->requests) {
            $this->reset();

            // Making sleep
            sleep($this->limit->interval);

            return true;
        }
        return false;
    }

    /**
     * Reset requests number and start time
     */
    public function reset()
    {
        $this->requests = 0;
    }
}